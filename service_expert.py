from string_input_f import StringInputF
from browse_f import BrowseF

import tkinter as tk
import subprocess


NORMAL_FONT = ('Serif', 12)
SERVICE_EXTENSION = '.sys'
SERVICE_BROWSER_ENTRY = [
    (
        'Service',
        '*' + SERVICE_EXTENSION
    )
]


class ServiceExpert:

    def __init__(self, parent):

        self._service_browse = BrowseF(
            parent=parent,

            no_selection_message='Please select a service.',
            user_instruction='Select a service',

            browse_window_title='Service selection',
            initial_path='~',
            supported_files=SERVICE_BROWSER_ENTRY,

            browse_button_eh=self._service_file_selected_eh,

            directory=False,
            disabled=False
        )

        self._service_name_input = StringInputF(
            parent,

            user_instruction='Please write the service\'s name',

            input_changed_valid_eh=self._valid_service_name_eh,
            input_changed_invalid_eh=self._invalid_service_name_eh,

            allow_space=False,

            disabled=False
        )

        self._create_service_browse = tk.Button(
            parent,
            width=20,
            height=2,
            font=NORMAL_FONT,
            text='Create service',
            command=self._create_service_eh
        )

        self._start_service_browse = tk.Button(
            parent,
            width=20,
            height=2,
            font=NORMAL_FONT,
            text='Start service',
            command=self._start_service_eh
        )

        self._stop_service_browse = tk.Button(
            parent,
            width=20,
            height=2,
            font=NORMAL_FONT,
            text='Stop service',
            command=self._stop_service_eh
        )

        self._delete_service_browse = tk.Button(
            parent,
            width=20,
            height=2,
            font=NORMAL_FONT,
            text='Delete service',
            command=self._delete_service_eh
        )

        self._display_elements()

        self._path_to_service = ''
        self._service_name = ''

        self._created = False
        self._started = False

        self._disable()

    #########################################################################
    # Static methods

    @staticmethod
    def _run_command(
            command_text):
        try:
            result = subprocess.run(str(command_text), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            print('STDOUT :  ' + result.stdout.decode('utf-8'))
            print('STDERR :  ' + result.stderr.decode('utf-8'))
        except Exception as error:
            print('\n\nFailed to run the command!')
            print('Command :  ' + str(command_text))
            print(str(error) + '\n')

    @staticmethod
    def _pack_row(row):
        row.pack(
            side='top',
            fill='both',
            expand=False
        )

    @staticmethod
    def _pack_button(button):
        button.pack(
            side='top',
            fill='y',
            pady=15,
            expand=False
        )

    #########################################################################
    # Feature implementations

    def _create_service(self):
        print("Creating the service.")
        self._run_command('sc create ' + self._service_name + ' binPath="' + self._path_to_service + '" type=kernel')

        self._created = True
        self._started = False

    def _start_service(self):
        print("Starting the service.")
        self._run_command('sc start ' + self._service_name)

        self._created = True
        self._started = True

    def _stop_service(self):
        print("Stopping the service.")
        self._run_command('sc stop ' + self._service_name)

        self._created = True
        self._started = False

    def _delete_service(self):
        print("Deleting the service.")
        self._run_command('sc delete ' + self._service_name)

        self._created = False
        self._started = False

    #########################################################################
    # Event handling

    def _service_file_selected_eh(
            self,
            selected_path):

        print('Path of the selected service :  ' + str(self._path_to_service) + '\n')
        self._path_to_service = selected_path

    def _create_service_eh(
            self):
        self._create_service()

        self._create_service_browse.config(state='disabled')
        self._start_service_browse.config(state='normal')
        self._stop_service_browse.config(state='disabled')
        self._delete_service_browse.config(state='normal')

    def _start_service_eh(self):
        self._start_service()

        self._create_service_browse.config(state='disabled')
        self._start_service_browse.config(state='disabled')
        self._stop_service_browse.config(state='normal')
        self._delete_service_browse.config(state='disabled')

    def _stop_service_eh(self):
        self._stop_service()

        self._create_service_browse.config(state='disabled')
        self._start_service_browse.config(state='disabled')
        self._stop_service_browse.config(state='disabled')
        self._delete_service_browse.config(state='normal')

    def _delete_service_eh(self):
        self._delete_service()

        self._create_service_browse.config(state='normal')
        self._start_service_browse.config(state='disabled')
        self._stop_service_browse.config(state='disabled')
        self._delete_service_browse.config(state='disabled')

    def _valid_service_name_eh(
            self,
            _service_name):

        self._service_name = _service_name
        self._enable()

    def _invalid_service_name_eh(self):

        self._service_name = ''
        self._disable()

    #########################################################################
    # GUI

    def _display_elements(self):
        self._pack_row(self._service_browse)
        self._pack_row(self._service_name_input)

        self._pack_button(self._create_service_browse)
        self._pack_button(self._start_service_browse)
        self._pack_button(self._stop_service_browse)
        self._pack_button(self._delete_service_browse)

    def _enable(self):
        self._create_service_browse.config(state='normal')
        self._start_service_browse.config(state='disabled')
        self._stop_service_browse.config(state='disabled')
        self._delete_service_browse.config(state='disabled')

    def _disable(self):
        self._create_service_browse.config(state='disabled')
        self._start_service_browse.config(state='disabled')
        self._stop_service_browse.config(state='disabled')
        self._delete_service_browse.config(state='disabled')

    #########################################################################
    # Public methods

    def clean_up(self):
        if self._started:
            self._stop_service()

        if self._created:
            self._delete_service()

    #########################################################################
