from service_expert import ServiceExpert

import tkinter as tk


def exit_app():
    root.destroy()


def on_closing():
    exit_app()
    service_expert.clean_up()


root = tk.Tk()


root.title("Lazy Services")
root.protocol("WM_DELETE_WINDOW", on_closing)

RWidth = 800
RHeight = 470
root.geometry('%dx%d' % (RWidth, RHeight))

root.update()

root.resizable(False, False)

service_expert = ServiceExpert(root)

root.mainloop()
