import re

# Remove if you don't want to bother with PIL & PILLOW  -  START  -  1 out of 2
from PIL import Image, ImageTk


#############################################################################

def load_and_resize_image(
        image_path_file_name: str,
        width: int,
        height: int):

    image = Image.open(image_path_file_name)
    resized = image.resize((width, height))

    return ImageTk.PhotoImage(resized)

# Remove if you don't want to bother with PIL & PILLOW  -  END  -  1 out of 2

#############################################################################


def string_is_valid(
        string_to_check: str,
        allowed_characters: str):

    not_allowed_characters = re.sub(allowed_characters, '', string_to_check)

    return not_allowed_characters == '' and string_to_check != ''


#############################################################################
