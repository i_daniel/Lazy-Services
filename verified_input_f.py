from validity_indicator_f import ValidityIndicatorF

import tkinter as tk


class VerifiedInputF(tk.Frame):

    def __init__(self,

                 parent,

                 validation_method,

                 valid_input_eh,
                 invalid_input_eh,

                 form_id=None,
                 disabled=False,

                 input_entry_width=50):

        tk.Frame.__init__(self,
                          parent,
                          padx=1,
                          pady=6)

        self._input_entry_width = input_entry_width
        self._validation_method = validation_method

        if form_id is not None and not isinstance(form_id, int):
            raise TypeError('form_if must be int')

        self._form_id = form_id
        self._valid_input_eh = valid_input_eh
        self._invalid_input_eh = invalid_input_eh

        self._create_widgets()
        self._place_widgets()

        self._input = None

        if disabled:
            self.disable()

    #########################################################################
    # Widget creation and placement

    def _create_widgets(self):

        _sv = tk.StringVar()
        _sv.trace("w", lambda name, index, mode, sv=_sv: self._input_changed(sv))

        self._e_input = tk.Entry(
            self,

            width=self._input_entry_width,

            font=('Serif', 12),
            justify='center',
            textvariable=_sv
        )

        self._vi = ValidityIndicatorF(self)

    def _place_widgets(self):

        self._e_input.pack(side='left',
                           fill='both',
                           expand=True)
        self._vi.pack(side='left',
                      fill='both',
                      expand=True)

    #########################################################################
    # Event handling

    def _input_changed(
            self,
            sv: tk.StringVar):
        self._input = sv.get()

        if self._form_id is not None:
            self.check_input_id()
        else:
            self.check_input()

    #########################################################################
    # Public methods

    def get_input(self):

        return self._input

    def check_input_id(self):

        if self._validation_method(self.get_input()):
            self._vi.valid()

            self._valid_input_eh(self._form_id)
        else:
            self._vi.invalid()

            self._invalid_input_eh(self._form_id)

    def check_input(self):

        if self._validation_method(self.get_input()):
            self._vi.valid()

            self._valid_input_eh()
        else:
            self._vi.invalid()

            self._invalid_input_eh()

    def valid(self):

        self._vi.valid()

    def invalid(self):

        self._vi.invalid()

    def enable(self):

        self._e_input.config(state='normal')
        self._vi.enable()

    def disable(self):

        self._vi.disable()
        self._e_input.config(state='disabled')

    #########################################################################
