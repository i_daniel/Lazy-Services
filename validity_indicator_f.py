import utils as utils
import tkinter as tk


class ValidityIndicatorF(tk.Frame):

    def __init__(self,

                 parent,

                 disabled=False):

        tk.Frame.__init__(self,
                          parent)

        self._load_images()

        self._lbl_indicator = tk.Label(
            self,

            text=self._invalid_txt,
            image=self._img_invalid,
            font=('Serif', 8, 'italic')
        )

        self._lbl_indicator.image = self._img_invalid

        self._lbl_indicator.pack(side='right',
                                 fill='both',
                                 expand=True)

        if disabled:
            self.disable()

    #########################################################################
    # Image handling

    def _load_images(self):

        # Remove if you don't want to bother with PIL & PILLOW  -  START  -  2 out of 2
        try:
            self._img_valid = utils.load_and_resize_image(
                image_path_file_name='./images/_good.png',
                height=30,
                width=30
            )

            self._img_invalid = utils.load_and_resize_image(
                image_path_file_name='./images/_bad.png',
                height=30,
                width=30
            )

            self._valid_txt = ''
            self._invalid_txt = ''
        except FileNotFoundError:
            # Remove if you don't want to bother with PIL & PILLOW  -  END  -  2 out of 2
            self._failed_to_load_images()

    def _failed_to_load_images(self):

        self._invalid_txt = 'Invalid'
        self._valid_txt = 'Valid'

        self._img_invalid = None
        self._img_valid = None

    #########################################################################
    # Public methods

    def valid(self):

        self._lbl_indicator.config(
            text=self._valid_txt,
            image=self._img_valid
        )

        self._lbl_indicator.image = self._img_valid

    def invalid(self):

        self._lbl_indicator.config(
            text=self._invalid_txt,
            image=self._img_invalid
        )

        self._lbl_indicator.image = self._img_invalid

    def enable(self):

        self._lbl_indicator.config(state='normal')

    def disable(self):

        self._lbl_indicator.config(state='disabled')

    #########################################################################
