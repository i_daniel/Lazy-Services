# Lazy-Services

Use this application to create, start, stop and delete services.


## Installation

- Unzip **[lazy_services_app.zip](https://github.com/dani-i/Lazy-Services/blob/master/lazy_services_app.zip)** and run **lazy_services_app.exe**.


## Notes

- Tested with Python 3.6.4

- If you don't want to use the *.exe* and you don't want to bother with PIL & PILLOW, since it's only for the looks, you can remove the PIL & PILLOW dependencies from [utils.py](https://github.com/dani-i/Lazy-Services/blob/master/utils.py) and [validity_indicator_f.py](https://github.com/dani-i/Lazy-Services/blob/master/validity_indicator_f.py), more details in the files.

- **Important** On closing **the last** (started) created **service will be (*STOPPED* and) *DELETED*** automatically.


## Authors

* **Iliesi Daniel** - [dani-i](https://github.com/dani-i).


## License

This project is licensed under the Apache-2.0 License - see the [LICENSE.md](LICENSE.md) file for details.
